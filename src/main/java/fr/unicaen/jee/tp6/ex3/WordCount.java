/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unicaen.jee.tp6.ex3;

import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.nio.charset.*;
import java.util.stream.*;
import java.util.function.*;

/**
 * @author Frédérik Bilhaut
 */
public class WordCount {
	
	public static Map<String, Long> compute(File file, Charset charset, Function<String,String[]> tokenize, Predicate<String> filter, Function<String,String> normalize) throws IOException {
	
		return Files.lines(file.toPath(), charset)
			.map(tokenize)
			.flatMap(Arrays::stream)
			.filter(filter)
			.collect(Collectors.groupingBy(normalize, Collectors.counting()));	
	}
	
}
