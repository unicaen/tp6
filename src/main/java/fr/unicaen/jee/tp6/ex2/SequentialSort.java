package fr.unicaen.jee.tp6.ex2;

import java.util.*;
import java.util.concurrent.*;


/**
 * Version non parallèle pour vérifier l'algorithme de base
 * 
 * @author Frédérik Bilhaut
 */
public class SequentialSort<T extends Comparable<T>> {
			
		
	public List<T> sort(List<T> list) throws InterruptedException, ExecutionException {
		
		if(list.size() <= 1)
			return list;
		
		// Attention, chaque branche du calcul doit travailler sur la propre copie
		// D'où la copie dans une nouvelle liste car subList() ne fait pas de copie profonde.
		List<T> l1 = new LinkedList<>(list.subList(0, list.size() / 2));
		List<T> l2 = new LinkedList<>(list.subList(list.size() / 2, list.size()));
		
		List<T> f1 = new MyCallable(l1).call();
		List<T> f2 = new MyCallable(l2).call();
		
		return Fusion.fusionne(f1, f2);		
	}
	
	
	private class MyCallable implements Callable<List<T>> {
		
		private final List<T> data;
		
		public MyCallable(List<T> data) {
			this.data = data;
		}
		
		@Override
		public List<T> call() throws InterruptedException, ExecutionException {
			return sort(this.data);
		}
	}

}