package fr.unicaen.jee.tp6.ex2;

import java.util.*;
import java.util.concurrent.*;


/**
 * @author Frédérik Bilhaut
 */
public class NaiveParallelSort<T extends Comparable<T>> {
		
	private final ExecutorService executor;
		
		
	public NaiveParallelSort() 
	{
		this.executor = Executors.newCachedThreadPool();
	}
	
	
	public List<T> sort(List<T> list) throws InterruptedException, ExecutionException {
		
		if(list.size() <= 1)
			return list;
		
		// Attention, chaque branche du calcul doit travailler sur la propre copie
		// D'où la copie dans une nouvelle liste car subList() ne fait pas de copie profonde.
		List<T> l1 = new LinkedList<>(list.subList(0, list.size() / 2));
		List<T> l2 = new LinkedList<>(list.subList(list.size() / 2, list.size()));
		
		Future<List<T>> f1 = executor.submit(new MyCallable(l1));
		Future<List<T>> f2 = executor.submit(new MyCallable(l2));
		
		return Fusion.fusionne(f1.get(), f2.get());		
	}
	
	
	private class MyCallable implements Callable<List<T>> {
		
		private final List<T> data;
		
		public MyCallable(List<T> data) {
			this.data = data;
		}
		
		@Override
		public List<T> call() throws InterruptedException, ExecutionException {
			return sort(this.data);
		}
	}
	
	
	

}