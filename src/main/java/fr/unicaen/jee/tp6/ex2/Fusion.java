package fr.unicaen.jee.tp6.ex2;

import java.util.*;

/**
 *
 * @author Frédérik Bilhaut
 */
public class Fusion {
	
	
	public static <T extends Comparable<T>> List<T> fusionne(List<T> l1, List<T> l2)  {		
		
		List<T> result = new LinkedList<>();
		
		while(l1.size() > 0 || l2.size() > 0) {
			
			if(l1.isEmpty()) {
				result.addAll(l2);
				l2.clear();
			}				
			else if(l2.isEmpty()) {
				result.addAll(l1);
				l1.clear();
			}
			else if(l1.get(0).compareTo(l2.get(0)) < 0) {			
				result.add(l1.remove(0));
			}
			else {
				result.add(l2.remove(0));
			}
		}
		
		return result;			
	}
	
}
