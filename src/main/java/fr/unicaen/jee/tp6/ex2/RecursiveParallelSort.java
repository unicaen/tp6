package fr.unicaen.jee.tp6.ex2;

import java.util.*;
import java.util.concurrent.*;


/**
 * @author Frédérik Bilhaut
 */
public class RecursiveParallelSort<T extends Comparable<T>> {
		
	
	public List<T> sort(List<T> list) throws InterruptedException, ExecutionException {
		MyRecursiveTask callable = new MyRecursiveTask(list);
		ForkJoinPool forkJoinPool = new ForkJoinPool(10);
		forkJoinPool.invoke(callable);
		forkJoinPool.shutdown();
		return callable.getRawResult();
	}
	
	
	private class MyRecursiveTask extends RecursiveTask<List<T>> {
		
		private final List<T> input;		
		
		public MyRecursiveTask(List<T> data) {
			this.input = data;
		}		
		
		@Override
		protected List<T> compute() {
			if(input.size() <= 1)
				return this.input;							
					
			List<T> l1 = new LinkedList<>(this.input.subList(0, this.input.size() / 2));
			List<T> l2 = new LinkedList<>(this.input.subList(this.input.size() / 2, this.input.size()));

			MyRecursiveTask call1 = new MyRecursiveTask(l1);
			MyRecursiveTask call2 = new MyRecursiveTask(l2);
			invokeAll(call1, call2);

			return Fusion.fusionne(call1.getRawResult(), call2.getRawResult());		
		}
	}

}