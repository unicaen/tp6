package fr.unicaen.jee.tp6.ex1;


/**
 * @author Frédérik Bilhaut
 */
public interface Factory<T> {

	T create();
	
	void dispose(T t);

}