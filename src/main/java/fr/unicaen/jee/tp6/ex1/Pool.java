package fr.unicaen.jee.tp6.ex1;


/**
 * @author Frédérik Bilhaut
 */
public interface Pool<T> {
	
	T get() throws InterruptedException;
	
	void release(T t);
	
	void dispose() throws InterruptedException;

}