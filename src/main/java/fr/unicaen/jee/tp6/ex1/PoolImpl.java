package fr.unicaen.jee.tp6.ex1;

import java.util.*;


/**
 * @author Frédérik Bilhaut
 */
public class PoolImpl<T> implements Pool<T> {
	
	private final Queue<T> queue = new LinkedList<>();
	private final Factory<T> factory;
	private final int maxSize;
	private int count;
	private boolean closed;
			

	public PoolImpl(int maxSize, Factory<T> factory) {
		this.maxSize = maxSize;
		this.factory = factory;
		this.count = 0;
		this.closed = false;
	}

	
	@Override
	public synchronized T get() throws InterruptedException, IllegalStateException {		
		
		checkNotClosed();
		
		if(queue.size() > 0)
			return queue.remove();
	
		if(count < maxSize) {
			count++;
			return this.factory.create();
		}
	
		while(this.queue.isEmpty())
			wait();
		
		checkNotClosed(); // Il peut avoir été fermé entre temps
	
		return this.queue.remove();
	}


	@Override
	public synchronized void release(T t) {
		this.queue.add(t);
		notifyAll();
	}


	@Override
	public synchronized void dispose() throws InterruptedException {
		if(this.closed)
			return;
		
		this.closed = true;
		
		while(this.queue.size() != count)
			wait();
		
		for(T t : queue)
			this.factory.dispose(t);
		
		this.queue.clear();		
	}
	
	private synchronized void checkNotClosed() throws IllegalStateException {
		if(this.closed)
			throw new IllegalStateException("Ce pool est fermé et ne délivre plus d'objets");
	}

}