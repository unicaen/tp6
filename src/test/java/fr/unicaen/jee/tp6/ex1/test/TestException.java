package fr.unicaen.jee.tp6.ex1.test;


/**
 * @author Frédérik Bilhaut
 */
class TestException extends Exception {

	public TestException(Throwable t) {
		super(t);
	}
	
}
