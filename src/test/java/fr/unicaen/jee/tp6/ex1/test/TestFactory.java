package fr.unicaen.jee.tp6.ex1.test;

import fr.unicaen.jee.tp6.ex1.*;


/**
 * @author Frédérik Bilhaut
 */
public class TestFactory implements Factory<TestObject> {

	public TestObject create() {
		return new TestObject();
	}

	public void dispose(TestObject t) {
		t.dispose();
	}
	
	
	
}
