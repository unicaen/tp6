package fr.unicaen.jee.tp6.ex1.test;

import org.junit.*;
import fr.unicaen.jee.tp6.ex1.*;

/**
 * @author Frédérik Bilhaut
 */
public class PoolTest {
	
	@Test
	public void test1() throws InterruptedException {
		
		final int POOL_SIZE = 2;
		final int THREADS_COUNT = 5;
		
		Pool<TestObject> pool = new PoolImpl<>(POOL_SIZE, new TestFactory());
		
		Thread[] threads = new Thread[THREADS_COUNT];
		for(int i=0; i<THREADS_COUNT; i++) {
			threads[i] = new Thread(new TestRunnable(pool));
			threads[i].start();
		}
		
		for(Thread thread : threads) {
			thread.join();
		}
		
		pool.dispose();
		
	}
	
	
	private static class TestRunnable implements Runnable {
		private final Pool<TestObject> pool;
				
		public TestRunnable(Pool<TestObject> pool) {
			this.pool = pool;
		}
				
		@Override
		public void run() {
			try {
				for(int i=0; i<10; i++) {
					TestObject obj = this.pool.get();
					obj.use();
					this.pool.release(obj);
				}
			}
			catch(TestException|InterruptedException e) {				
				System.err.println(Thread.currentThread().getName() + ": " + e.getMessage());
			}
		}
		
	}
	
}
