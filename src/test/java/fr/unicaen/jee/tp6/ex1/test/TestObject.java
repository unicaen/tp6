package fr.unicaen.jee.tp6.ex1.test;


/**
 * @author Frédérik Bilhaut
 */
public class TestObject {
	
	public void use() throws TestException {
		try {
			System.out.println("La thread " + Thread.currentThread().getName() + " utilise l'objet " + this);
			Thread.sleep((long)(Math.random() * 50));
			System.out.println("La thread " + Thread.currentThread().getName() + " a terminé d'utiliser l'objet " + this);
		}
		catch(InterruptedException e) {
			throw new TestException(e);
		}
	}

	
	void dispose() {
		System.out.println("L'objet " + this + " a été 'disposé");
	}
	
}
