package fr.unicaen.jee.tp6.ex2.test;

import java.util.*;
import org.junit.*;
import fr.unicaen.jee.tp6.ex2.*;


/**
 * @author Frédérik Bilhaut
 */
public class SortTest {
	
	@Test
	public void test1() throws Exception {
		SequentialSort<Integer> sort = new SequentialSort<>();
		
		List<Integer> list = new LinkedList<>(Arrays.asList(5, 54, 32, 42, 1, 0, -12));
		list = sort.sort(list);
		
		assertSorted(list);
	}
	
	
	@Test
	public void test2() throws Exception {
		NaiveParallelSort<Integer> sort = new NaiveParallelSort<>();
		
		List<Integer> list = Arrays.asList(5, 54, 32, 42, 1, 0, -12);		
		list = sort.sort(list);
		
		assertSorted(list);
	}
	
	
	@Test
	public void test3() throws Exception {
		SequentialSort<Integer> sortS = new SequentialSort<>();
		NaiveParallelSort<Integer> sortP = new NaiveParallelSort<>();
		
		List<Integer> list = new LinkedList<>();
		for(int i=0; i<1000; i++)
			list.add((int)(Math.random() * 1000));
				
		List resultS = sortS.sort(list);					
		assertSorted(resultS);
				
		List resultP = sortP.sort(list);				
		assertSorted(resultP);
	
		Assert.assertArrayEquals(resultS.toArray(), resultP.toArray());
	}
	
	
	@Test
	public void test4() throws Exception {
		RecursiveParallelSort<Integer> sort = new RecursiveParallelSort<>();
		
		List<Integer> list = Arrays.asList(5, 54, 32, 42, 1, 0, -12);		
		List<Integer> sorted = sort.sort(list);
		
		Assert.assertEquals((long)list.size(), (long)sorted.size());
		assertSorted(sorted);
	}
	
	
	@Test
	@Ignore
	public void test5() throws Exception {
		SequentialSort<Integer> sortS = new SequentialSort<>();
		RecursiveParallelSort<Integer> sortP = new RecursiveParallelSort<>();
		
		List<Integer> list = new LinkedList<>();
		for(int i=0; i<1000000; i++)
			list.add((int)(Math.random() * 10000));
		
		long timeS = System.currentTimeMillis();
		List resultS = sortS.sort(list);		
		timeS = System.currentTimeMillis() - timeS;
		Assert.assertEquals((long)list.size(), (long)resultS.size());
		assertSorted(resultS);
		
		long timeP = System.currentTimeMillis();
		List resultP = sortP.sort(list);			
		timeP = System.currentTimeMillis() - timeP;
		Assert.assertEquals((long)list.size(), (long)resultP.size());
		assertSorted(resultP);
			
		Assert.assertArrayEquals(resultS.toArray(), resultP.toArray());
		
		System.out.println(timeS + " / " + timeP);
	}
	
	
	private <T extends Comparable> void assertSorted(List<T> list) {
		T last = null;
		for(T t : list) {
			Assert.assertTrue(last == null || last.compareTo(t) <= 0);
			last = t;
		}
	}
	
}
