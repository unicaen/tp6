package fr.unicaen.jee.tp6.ex3.test;

import java.io.*;
import java.nio.charset.*;
import org.junit.*;
import fr.unicaen.jee.tp6.ex3.*;

/**
 *
 * @author Frédérik Bilhaut
 */
public class WordCountTest {
	
	@Test
	public void test1() throws IOException {
		System.out.println(
			WordCount.compute(
				new File("README.md"), 
				StandardCharsets.UTF_8,
				x -> x.split("[\\s\\p{Punct}]+"),
				x -> x.length() > 2,
				x -> x.toLowerCase())
		);
	}
	
}
